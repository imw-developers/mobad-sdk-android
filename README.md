## MobAd SDK

Welcome to **MobAd SDK**, designed to enhance your Android app with powerful advertising
capabilities. This guide will walk you through the process of integrating the SDK into your Android
Studio project. By following these steps, you'll be up and running in no time.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
  - [Instantiate the MobAd Class](#instantiate-the-mobad-class)
  - [Set MobAd State](#set-mobad-state)
  - [User Initialization](#user-initialization)
  - [Get Ad](#get-ad)
- [Push Notifications](#push-notifications)
  - [GMS](#gms)
  - [HMS](#hms)
- [Additional functions](#additional-functions)
  - [User Interests](#user-interests)
  - [Ad Service State](#ad-service-state)
  - [User Ad Cap](#user-ad-cap)
  - [User Ad languages](#user-ad-languages)
- [Banner Ad](#banner-ad)
- [Native Ad](#native-ad)
  - [Native Ad with RecyclerView](#native-ad-with-recyclerview)
- [Contact](#contact)

## Installation

**Step 1**. Add the JitPack repository to your build file.

Add it in your root build.gradle at the end of repositories.

```
allprojects {
    repositories {
        ...
        maven { url 'https://jitpack.io' }
    }
}
```

**Step 2**. Set the provided App ID and Machine Secret Key.

In your app's build.gradle file, add the following lines within the defaultConfig block.
Replace `<your_app_id>` and `<your_machine_secret>` with your actual App ID and Machine Secret Key,
respectively.

```
android {
    ...
    defaultConfig {
        ...
        manifestPlaceholders = [
            mobad_app_id        : "<your_app_id>",
            mobad_machine_secret: "<your_machine_secret>"
        ]
    }
}
```

**Step 3**. Add the SDK dependency.

Add the following line to the dependencies block in your app's build.gradle file. Replace x.x.x with
the latest version of the SDK.

Find the latest version here. [![](https://jitpack.io/v/com.gitlab.imw-developers/mobad-sdk-android.svg)](https://jitpack.io/#com.gitlab.imw-developers/mobad-sdk-android)

```
dependencies {
    implementation 'com.gitlab.imw-developers:mobad-sdk-android:x.x.x'
}
```

**Note**: Two versions of the SDK are available:

* `x.x.x` for **Google-supported devices**.
* `x.x.x-hms` for **Huawei-supported devices**.

## Usage

### Instantiate the MobAd Class

To use the MobAd SDK, you need to instantiate the `MobAd` class. Use the following code in Kotlin or
Java:

```kotlin
val mobAd = MobAd(context)
```

```java
MobAd mobAd = new MobAd(context);
```

---

### Set MobAd State

This function switches between the development server and production server. By default, the SDK is
set to the development server.

**Note**: **Don't forget to change it to the production before launch your app!**

```kotlin
mobAd.setMobAdState(/** set the MobAdState here **/)
```

**Note**: MobAdState is an Enum containing two constants:

* PRODUCTION
* DEVELOPMENT

---

### User Initialization

Initialize the user in the application; this should be called at least once in the app lifecycle.
Use the following code in Kotlin or Java:

```kotlin
mobAd.initializeUser(languageCode, onSuccess = {
    //Todo on success
}, onError = { message ->
    //Todo on error
})
```

```java
mobAd.initializeUser(languageCode,new InitializeCompletionListener(){
@Override
public void onSuccess(){
    //Todo on success
}

@Override
public void onError(@Nullable String message){
    //Todo on error
}
});
```

**Note**: Language code is the ISO 639-1 2-letter code of the user's language. If null, the SDK
detects the app or phone default language.

---

### Get Ad

Retrieve an ad and display it to users immediately.

```kotlin
mobAd.getAd()
```

Optional parameters for `getAd` function:

- `delayTimeInMillis`: Pass a `long` value to delay the execution of the function before getting
  the ad, sending it null will show the ad immediately.
- `fullScreenAdType`: Pass a FullScreenAdType Enum value to request a specific ad type, sending it
  null will get a random Ad.
- `adEventListener`: Pass an AdEventListener interface implementation to listen for the ad
  events like opened/closed/failed, you can send it as null.
- `forceFullScreen`: A `boolean` value to force full screen view without bubble or local
  notification.

```kotlin
mobAd.getAd(
    /** your long goes here **/,
    /** your desired type goes here **/,
    /** your adEventListener object goes here **/,
    /** your boolean goes here **/
)
```

**Notes**:

* This function checks for draw over other apps permission on every call (Until the user denies it
  forever).
* The draw over other apps permission is used to show the bubble advertisements only.

## Push Notifications

It is necessary to call the `initializeUser` function to activate push notifications.
For more details, check [User Initialization](#user-initialization).

Sample code for handling notifications:

#### GMS

No action is required when using Google Mobile Services, simply call `initializeUser` to activate
push notifications.

---

#### HMS

You have to [integrate your app](https://developer.huawei.com/consumer/en/doc/HMSCore-Guides/android-integrating-sdk-0000001050040084)
with Huawei Push Kit first and add our `sender ID` to your account, Request our `sender ID` and add
it to your project's PushKit settings as a multi-sender.

In addition, your MessagingService onMessageReceived will be called, so you need to call our class
for the messages coming from us.

In order to do so, you will have to check for the availability of the key `isMobad` in
the JSON payload of the notification. The presence of this key will indicate that it's a
notification coming from our side.

We created a static field MobAd.mobAdNotificationKey that can used to check if the notification
belongs to us.

```kotlin
override fun onMessageReceived(remoteMessage: RemoteMessage) {
    super.onMessageReceived(remoteMessage)
    if (remoteMessage.dataOfMap.containsKey(MobAd.mobAdNotificationKey)) {
        MobAd.onMessageReceived(context, remoteMessage.dataOfMap)
        return
    }
    ...
    Your code goes here
    ...
}
```

```java
@Override
public void onMessageReceived(@NonNull RemoteMessage remoteMessage){
    super.onMessageReceived(remoteMessage);
    if (remoteMessage.getDataOfMap().containsKey(MobAd.mobAdNotificationKey)) {
        MobAd.RemoteNotification.onMessageReceived(this, remoteMessage.getDataOfMap());
        return;
    }
    ...
    Your code goes here
    ...
}
```

## Additional functions

### User Interests

To get categories, subcategories, and whether the user is interested in the subcategory (found in
the isUserInterested field in the Subcategory). Use the following code in Kotlin or Java:

```kotlin
mobAd.getCategories(onSuccess = { categories: List<Category> ->
    //Todo on success
}, onError = { message ->
    //Todo on error
})
```

```java
mobAd.getCategories(new GetCategoriesCompletionListener(){
@Override
public void onSuccess(List<Category> categories){
        //Todo on success
        }

@Override
public void onError(@Nullable String message){
        //Todo on error
        }
});
```

**To add or remove interests:**

```kotlin
mobAd.addInterests(subcategories, onSuccess = {
    //Todo on success
}, onError = { message ->
    //Todo on error
})

mobAd.removeInterests(subcategories, onSuccess = {
  //Todo on success
}, onError = { message ->
  //Todo on error
})
```

```java
mobAd.addInterests(subcategories,new UpdateInterestsCompletionListener(){
@Override
public void onSuccess(){
        //Todo on success
        }

@Override
public void onError(@Nullable String message){
        //Todo on error
        }
});

mobAd.removeInterests(subcategories,new UpdateInterestsCompletionListener(){
@Override
public void onSuccess(){
        //Todo on success
        }

@Override
public void onError(@Nullable String message){
        //Todo on error
        }
});
```

**Note**: The subcategories that should be sent for add/remove functions are sent in a list of
Subcategory objects in getCategories function.

### Ad Service State

To get the current ad service state and disable/enable the ad service:

```kotlin
mobAd.getAdsEnabled()
mobAd.setAdsEnabled(/** your boolean here **/) //true to enable - false to disable
```

### User Ad Cap

In order to set the maximum ads per day

```kotlin
mobAd.setMaximumAdsPerDay(/** your int value here **/)
```

**Note**: The maximum number that can be added is 50.

In order to get the current ad cap

```kotlin
mobAd.getMaximumAdsPerDay()
```

In order to get the number of opened ads per the day

```kotlin
mobAd.getOpenedAdsPerDay()
```

### User Ad languages

In order to get the supported languages, and whether the user have this language selected to get ads
at or not (found in the isSelected field in the Language object) (by default the user prefers to
view ads that are the same as his phone locale or the language selected during initialization)

**Kotlin**

```kotlin
mobAd.getAdLanguages(onSuccess = { languages: List<Language> ->
    //Todo on success
}, onError = { message ->
    //Todo on error
})
```

**Java**

```java
mobAd.getAdLanguages(new GetAdLanguagesCompletionListener(){
@Override
public void onSuccess(List<Language> languages){
        //Todo on success
        }

@Override
public void onError(@Nullable String message){
        //Todo on error
        }
});
```

In order to update the preferred languages

**Kotlin**

```kotlin
mobAd.updateAdLanguages(languages, onSuccess = { updatedLanguages: List<String> ->
    //Todo on success
}, onError = { message ->
    //Todo on error
})
```

**Java**

```java
mobAd.updateAdLanguages(languages,new UpdateAdLanguagesCompletionListener(){
@Override
public void onSuccess(updatedLanguages:List<String>){
        //Todo on success
        }

@Override
public void onError(@Nullable String message){
        //Todo on error
        }
});
```

The languages that should be sent for update function are sent in a list of Language objects in
getAdLanguages function.

## Banner Ad

To add banner view to your screen:
 
**Using XML**

Add MobAdBanner view to your XML layout file:

```xml
<com.imagineworks.mobad_ui.view.banner_ad.MobAdBanner android:id="@+id/mobad_banner"
        android:layout_width="wrap_content" android:layout_height="wrap_content" />
```

In your view class, use MobAdRequest:

```kotlin
private lateinit var mobadBanner: MobAdBanner

override fun onCreate(savedInstanceState: Bundle?) {
  super.onCreate(savedInstanceState)
  mobadBanner = findViewById(R.id.mobad_banner)
  MobAdRequest.Builder(context)
    .mobAdServerState(MobAdState.DEVELOPMENT).build()
    .loadBannerAd(mobadBanner, ContentAdType.IMAGE)
}
```

**Note**: The size of MobAdBanner is 320x100

**Using Jetpack Compose**

```kotlin
AndroidView(
  factory = {
    AspectRatioFrameLayout(context).apply {
      val mobAdBanner = MobAdBanner(context)
      addView(mobAdBanner)
      MobAdRequest.Builder(context)
        .mobAdServerState(MobAdState.DEVELOPMENT).build()
        .loadBannerAd(mobAdBanner, ContentAdType.VIDEO)
    }
  }
)
```

**Note**: ContentAdType is an Enum class holds Ad content type, you can send it null and the banner
view type will be random.

**Don't forget to switch to the PRODUCTION server before releasing your app**

## Native Ad

To add native view to your screen:

**Using XML**

Add MobAdNative view to your XML layout file:

```xml
<com.imagineworks.mobad_ui.view.native_ad.MobAdNative android:id="@+id/mobad_native"
        android:layout_width="wrap_content" android:layout_height="150dp" />
```

**NOTE**: You can customize the width or height of the native ad according to your desire, keeping
in mind that the aspect ratio is 16:9.

In your view class, use MobAdRequest:

```kotlin
private lateinit var mobadNative: MobAdNative

override fun onCreate(savedInstanceState: Bundle?) {
  super.onCreate(savedInstanceState)
  mobadNative = findViewById(R.id.mobad_native)
  MobAdRequest.Builder(context).mobAdServerState(MobAdState.DEVELOPMENT).build()
    .loadNativeAd(mobadNative, ContentAdType.VIDEO)
}
```

**Using Jetpack Compose**

```kotlin
AndroidView(
  factory = {
    AspectRatioFrameLayout(context).apply {
      val mobAdNative = MobAdNative(context)
      addView(mobAdNative)
      MobAdRequest.Builder(context)
        .mobAdServerState(MobAdState.DEVELOPMENT).build()
        .loadNativeAd(mobAdNative, ContentAdType.IMAGE)
    }
  }
)
```

### Native Ad with RecyclerView

Use our RecyclerView Wrapper to add Native ads to your RecyclerView, add this function to your view
class and use it when your recyclerview is ready:

```kotlin
private fun insertAdViewsIntoRecyclerView(mLayoutManager: LayoutManager, mAdapter: clientAdapter) {
  val mobAdNativeAdAdapter = MobAdNativeAdapter.Builder
    .with(mAdapter, MobAdLoader(context, null, MobAdState.DEVELOPMENT))
    .adItemInterval(4)
    .isStartVideoMuted(true)
    .isAutoPlayVideo(false)
    .adIconPosition(ADIconPosition.TopStart)
    .build()
  clientRecyclerView.apply {
    layoutManager = mLayoutManager
    adapter = mobAdNativeAdAdapter
  }
}
```

**Notes**:
* `mLayoutManager`: can be any type of LayoutManager you need to use with your RecyclerView. 
* `mAdapter`: is your `RecyclerView.Adapter<VH>` class.

## Contact

That's all you need to do! If you require further assistance or have any questions, please contact
our support team at [support@i-magineworks.com](mailto:support@i-magineworks.com).